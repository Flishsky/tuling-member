﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TulingMember.Core
{
   public  class TimeHelper
    {
        /// <summary>
        /// 获取两个日期相差天数
        /// </summary>
        /// <param name="time1"></param>
        /// <param name="time2"></param>
        /// <returns></returns>
        public static int GetDays(DateTime time1, DateTime time2) {
            TimeSpan timeSpan = time2.Subtract(time1);
            return timeSpan.Days;
        }
    }
}
