﻿using System;

namespace TulingMember.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class cts_ProductType: DEntityTenant
    {
               
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
                
        /// <summary>
        /// 
        /// </summary>
        public long ParentId { get; set; }
        /// <summary>
        /// 所有父级Id
        /// </summary>
        public string ParentIds { get; set; }

        public int Sort { get; set; }
    }
}