﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace TulingMember.Core
{
    /// <summary>
    /// 。
    /// </summary> 
    public class cts_FinanceItem: DEntityTenant
    {
    
        /// <summary>
        /// 项目名称。
        /// </summary>
     
        public string Name { get; set; }


        /// <summary>
        /// 1.收入2.支出
        /// </summary>

        public int Type { get; set; }

    }
}