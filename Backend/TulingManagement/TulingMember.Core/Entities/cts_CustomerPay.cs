﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;

namespace TulingMember.Core
{
    /// <summary>
    /// 。
    /// </summary> 
    [TableAudit]
    public class cts_CustomerPay : DEntityTenant
    {
    
        /// <summary>
        /// 。
        /// </summary>
     
        public string OrderNo { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public DateTime? OrderDate { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public long CustomerId { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public string CustomerName { get; set; }

    
        /// <summary>
        /// 合计。
        /// </summary>
     
        public decimal Amount { get; set; }
        /// <summary>
        /// 合计。
        /// </summary>

        public decimal DiscountAmount { get; set; }

        /// <summary>
        /// 。
        /// </summary>

        public string Remark { get; set; }

    }
}