﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace TulingMember.Core
{
    /// <summary>
    /// 。
    /// </summary> 
    [TableAudit]
    public class cts_SaleOrderDetail: DEntityTenant
    {  
        /// <summary>
        /// 。
        /// </summary>
     
        public long OrderId { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public long ProductId { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public string ProductName { get; set; }

        /// <summary>
        /// 。
        /// </summary>

        public string ProductCode { get; set; }

        /// <summary>
        /// 。
        /// </summary>

        public string Unit { get; set; }

        public string Specification { get; set; }

        public int Num { get; set; }

        /// <summary>
        /// 。
        /// </summary>

        public decimal Weight { get; set; }

    
        /// <summary>
        /// 价格。
        /// </summary>
     
        public decimal Price { get; set; }

        /// <summary>
        /// 整包把数
        /// </summary>
        public int? SubNum { get; set; }

        /// <summary>
        /// 尾货把数
        /// </summary>
        public int? EndNum { get; set; }
        /// <summary>
        /// 合计。
        /// </summary>

        public decimal Amount { get; set; }

        /// <summary>
        /// 。
        /// </summary>

        public string Remark { get; set; }



    }
}