import { axios } from '@/libs/api.request'

export const errorReq = () => {
  return axios.get('error_url')
}

export const saveErrorLogger = info => {
  return axios.post('api/System/SaveErrorLogger', info)
}

export const GetSystemConfig = () => {
  return axios.get('api/System/GetSystemConfig')
}
export const SaveSystemConfig = form => {
  return axios.post('api/System/SaveSystemConfig', form)
}

export const GetPrintTag = type => {
  return axios.get('api/System/GetPrintTag/' + type)
}
export const SavePrintTag = form => {
  return axios.post('api/System/SavePrintTag', form)
}

export const DeletePrintTag = id => {
  return axios.post('api/System/DeletePrintTag/' + id)
}

export const GetHomeData = () => {
  return axios.get('api/Data/GetHomeData')
}
